import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Profile from "../views/Profile.vue";
import Aboutus from "../views/Aboutus.vue";
import Contact from "../views/Contact.vue";
import NotFound from "../views/NotFound.vue";

const routes = [
  {
    name: "Home",
    path: "/",
    component: Home,
  },
  {
    name: "Aboutus",
    path: "/about",
    component: Aboutus,
  },
  {
    name: "profile",
    path: "/profile/:name",
    component: Profile,
  },
  {
    name: "Contact",
    path: "/contact",
    component: Contact,
  },
  {
    name: "NotFound",
    path: "/:catchAll(.*)",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
